ThisBuild / version := "0.0.1"

ThisBuild / scalaVersion := "2.13.10"

lazy val root = (project in file("."))
  .settings(
    name := "shiny-bakery"
  )

resolvers += Resolver.DefaultMavenRepository
libraryDependencies += "net.katsstuff" %% "ackcord" % "0.18.1"
libraryDependencies += "com.typesafe.sbt" % "sbt-interface" % "0.13.15"
excludeDependencies ++= Seq(
  ExclusionRule("com.sedmelluq", "lavaplayer")
)