import ackcord._
import com.typesafe.config.ConfigFactory
import org.slf4j.{Logger, LoggerFactory}
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, MILLISECONDS}

object Bootstrap extends App {
  val logger = LoggerFactory.getLogger(this.getClass)

  val config = ConfigFactory.load("application.conf")
  val token = config.getString("shiny-bakery.token")

  val clientSettings = ClientSettings(token)
  val client = Await.result(clientSettings.createClient(), Duration(config.getInt("shiny-bakery.client.timeout-duration"), MILLISECONDS))

  client.onEventSideEffectsIgnore {
    case _: APIMessage.Ready => logger.info("Bot initiated")
  }

  client.login()
}
